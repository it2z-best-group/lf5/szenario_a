DROP DATABASE IF EXISTS krautundrueben;
CREATE DATABASE IF NOT EXISTS krautundrueben;
USE krautundrueben;
CREATE TABLE `Address` (
    id INT PRIMARY KEY AUTO_INCREMENT,
    street VARCHAR(50),
    house_number VARCHAR(5),
    zip VARCHAR(5),
    city VARCHAR(50),
    telephone_number VARCHAR(15),
    email VARCHAR(50)
);
CREATE TABLE Customer (
    id INT PRIMARY KEY,
    last_name VARCHAR(50),
    first_name VARCHAR(50),
    birth_date DATE,
    FOREIGN KEY (id) REFERENCES `Address`(id) ON DELETE CASCADE
);
CREATE TABLE Supplier (
    id INT PRIMARY KEY,
    `name` VARCHAR(50),
    FOREIGN KEY (id) REFERENCES `Address`(id) ON DELETE CASCADE
);
CREATE TABLE Ingredient(
    id INT PRIMARY KEY AUTO_INCREMENT,
    supplier_id INT,
    FOREIGN KEY (supplier_id) REFERENCES Supplier(id) ON DELETE CASCADE,
    `name` VARCHAR(50),
    unit VARCHAR (25),
    price DECIMAL(10, 2),
    stock INT
);
CREATE TABLE `Order`(
    id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES Customer(id) ON DELETE CASCADE,
    date DATE,
    invoiced_amount DECIMAL(10, 2)
);
CREATE TABLE OrderIngredient (
    id INT PRIMARY KEY AUTO_INCREMENT,
    ingredient_id INT NOT NULL,
    FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id) ON DELETE CASCADE,
    order_id INT NOT NULL,
    FOREIGN KEY (order_id) REFERENCES `Order`(id) ON DELETE CASCADE,
    amount INT
);
CREATE TABLE Recipe (
    id INT PRIMARY KEY AUTO_INCREMENT,
    `name` varchar(50) NOT NULL
);
CREATE TABLE RecipeAmount (
    id INT PRIMARY KEY AUTO_INCREMENT,
    ingredient_id INT NOT NULL,
    FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id) ON DELETE CASCADE,
    recipe_id INT NOT NULL,
    FOREIGN KEY (recipe_id) REFERENCES Recipe(id) ON DELETE CASCADE,
    amount INT NOT NULL
);
CREATE TABLE Allergenic (
	id INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL
);
CREATE TABLE RecipeAllergenic (
	id INT PRIMARY KEY AUTO_INCREMENT,
	recipe_id INT NOT NULL,
	FOREIGN KEY (recipe_id) REFERENCES Recipe(id) ON DELETE CASCADE,
	allergenic_id INT NOT NULL,
	FOREIGN KEY (allergenic_id) REFERENCES Allergenic(id) ON DELETE CASCADE
);
CREATE TABLE NutritionalCategories (
	id INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL
);
CREATE TABLE RecipeNutritionalCategories (
	id INT PRIMARY KEY AUTO_INCREMENT,
	recipe_id INT NOT NULL,
	FOREIGN KEY (recipe_id) REFERENCES Recipe(id) ON DELETE CASCADE,
	nutritionalcategories_id INT NOT NULL,
	FOREIGN KEY (nutritionalcategories_id) REFERENCES NutritionalCategories(id) ON DELETE CASCADE
);
CREATE TABLE Nutrition(
    id INT PRIMARY KEY AUTO_INCREMENT,
    ingredient_id INT NOT NULL,
    FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id) ON DELETE CASCADE,
    calories DECIMAL(10, 2) NOT NULL,
    protein DECIMAL(10, 2) NOT NULL,
    carbohydrates DECIMAL(10, 2) NOT NULL,
    fat DECIMAL(10, 2) NOT NULL,
    fiber DECIMAL(10, 2) NOT NULL,
    sodium DECIMAL(10, 2) NOT NULL
);
